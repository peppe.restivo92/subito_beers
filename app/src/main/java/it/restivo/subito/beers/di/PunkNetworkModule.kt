package it.restivo.subito.beers.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import it.restivo.subito.beers.data.remote.PunkApi
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PunkNetworkModule {

    private const val NETWORK_TIMEOUT_SECONDS = 30L

    @Provides
    @Singleton
    fun provideProfileApi(moshi: Moshi, client: OkHttpClient.Builder): PunkApi {
        return Retrofit.Builder()
            .baseUrl(PunkApi.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(client.build())
            .build()
            .create(PunkApi::class.java)
    }

    @Reusable
    @Provides
    fun provideOkHttpClientBuilder(): OkHttpClient.Builder {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        val dispatcher = Dispatcher().apply {
            maxRequests = 1
        }

        return OkHttpClient.Builder().apply {
            this.addInterceptor(loggingInterceptor)
            this.dispatcher(dispatcher)
            this.connectTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            this.readTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            this.writeTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        }
    }
}