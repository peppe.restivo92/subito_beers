package it.restivo.subito.beers

import it.restivo.subito.beers.utils.brewedYearMonth
import it.restivo.subito.beers.utils.toBrewedFormat
import it.restivo.subito.beers.utils.zone
import junit.framework.Assert.fail
import org.junit.Assert
import org.junit.Test
import java.time.DateTimeException
import java.time.LocalDate
import java.util.*


class AndroidExtsUnitTest {

    @Test
    fun testBrewedYearMonth() {
        var a = "01-2022".brewedYearMonth()
        Assert.assertNotNull(a)
        Assert.assertEquals(1, a!!.monthValue)
        Assert.assertEquals(2022, a.year)

        a = "02-2022".brewedYearMonth()
        Assert.assertNotNull(a)
        Assert.assertEquals(2, a!!.monthValue)
        Assert.assertEquals(2022, a.year)
    }

    @Test
    fun testBrewedYearMonthWhenFormatIsNotCorrect() {
        var a = "01_2022".brewedYearMonth()
        Assert.assertNull(a)

        a = "01-22".brewedYearMonth()
        Assert.assertNotNull(a)
        Assert.assertEquals(1, a!!.monthValue)
        Assert.assertEquals(22, a.year)

        try {
            a = "13-22".brewedYearMonth()
            fail("Invalid value for MonthOfYear (valid values 1 - 12): 13")
        } catch (e: DateTimeException) {}
    }

    @Test
    fun testFromDateToBrewedYearMonth() {
        val d = Date.from(LocalDate.of(2022, 1, 1).atStartOfDay(zone).toInstant())
        val brewed = d.toBrewedFormat()
        Assert.assertEquals("01-2022", brewed)
    }
}