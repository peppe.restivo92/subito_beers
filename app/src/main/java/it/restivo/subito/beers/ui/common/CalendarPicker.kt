package it.restivo.subito.beers.ui.common

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.window.Dialog
import com.dt.composedatepicker.SelectDateListener
import it.restivo.subito.beers.R
import it.restivo.subito.beers.utils.toDate
import java.time.YearMonth
import java.util.*


@Composable
fun CalendarPicker(
    showCalendar: Boolean,
    setShowCalendar: (Boolean) -> Unit,
    startAt: YearMonth? = null,
    minDate: YearMonth? = null,
    maxDate: YearMonth? = null,
    onDateSelected: (Date?) -> Unit,
) {
    if (showCalendar) {
        Dialog(onDismissRequest = { setShowCalendar(false) }) {
            ComposeCalendar(
                startAt = startAt?.toDate(),
                minDate = minDate?.toDate(),
                maxDate = maxDate?.toDate(),
                locale = Locale("en"),
                title = stringResource(R.string.select_date),
                listener = object : SelectDateListener {
                    override fun onDateSelected(date: Date) {
                        onDateSelected(date)
                    }

                    override fun onCanceled() {
                        onDateSelected(null)
                    }
                }
            )
        }
    }
}