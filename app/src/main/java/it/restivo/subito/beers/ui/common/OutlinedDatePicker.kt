package it.restivo.subito.beers.ui.common

import androidx.annotation.StringRes
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import it.restivo.subito.beers.utils.brewedYearMonth
import timber.log.Timber
import java.time.YearMonth
import java.util.*

@Composable
fun OutlinedDatePicker(
    @StringRes label: Int,
    value: String?,
    modifier: Modifier = Modifier,
    minDate: String? = null,
    maxDate: String? = null,
    onValueChange: (Date?) -> Unit
) {
    val (showCalendar, setShowCalendar) = remember {
        mutableStateOf(false)
    }
    CalendarPicker(
        showCalendar = showCalendar,
        setShowCalendar = setShowCalendar,
        startAt = value?.brewedYearMonth(),
        minDate = minDate?.brewedYearMonth(),
        maxDate = maxDate?.brewedYearMonth()
    ) {
        Timber.i("Selected: ${it.toString()}")
        onValueChange(it)
        setShowCalendar(false)
    }

    val interactionSource = remember { MutableInteractionSource() }
    val pressedState by interactionSource.collectIsPressedAsState()
    if (pressedState) {
        setShowCalendar(true)
    }
    OutlinedTextField(
        readOnly = true,
        value = value ?: "",
        label = {
            Text(stringResource(label))
        },
        interactionSource = interactionSource,
        trailingIcon = { Icon(imageVector = Icons.Filled.DateRange, contentDescription = "") },
        onValueChange = {},
        modifier = modifier
    )
}