package it.restivo.subito.beers.ui.beer_list.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import it.restivo.subito.beers.R
import it.restivo.subito.beers.domain.model.Beer
import it.restivo.subito.beers.ui.theme.BeersTheme


@Composable
fun BeerListItem(
    beer: Beer,
    cardHeightSize: Dp = 160.dp,
    onBeerClicked: () -> Unit = {}
) {
    Card {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.Top
        ) {
            Box(
                modifier = Modifier
                    .size(cardHeightSize / 2f, cardHeightSize)
                    .padding(5.dp)
            ) {
                Image(
                    painter = if (beer.imageUrl != null) rememberImagePainter(
                        beer.imageUrl,
                        builder = {
                            crossfade(true)
                            placeholder(R.drawable.ic_bottle_beer)
                        }
                    ) else painterResource(id = R.drawable.ic_bottle_beer),
                    contentDescription = null,
                    contentScale = ContentScale.FillHeight,
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
            Spacer(modifier = Modifier.width(12.dp))
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(cardHeightSize),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    Text(
                        text = beer.name,
                        style = MaterialTheme.typography.h4,
                        maxLines = 1,
                        overflow = TextOverflow.Clip
                    )
                    beer.tagline?.let {
                        Text(
                            text = it,
                            style = MaterialTheme.typography.body1,
                            maxLines = 1,
                            overflow = TextOverflow.Clip
                        )
                    }
                    beer.firstBrewed?.let {
                        Text(
                            text = it,
                            style = MaterialTheme.typography.caption,
                            fontWeight = FontWeight.Bold
                        )
                    }
                }
                Text(
                    text = beer.description ?: "",
                    style = MaterialTheme.typography.caption,
                    maxLines = 4,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BeerListItemPreview(
    @PreviewParameter(LoremIpsum::class, 50) loremIpsum: String
) {
    BeersTheme {
        BeerListItem(beer =
            Beer(
                id = 1,
                name = "Beer 01",
                tagline = "Tagline 01",
                firstBrewed = "07/2008",
                description = loremIpsum
            )
        )
    }
}