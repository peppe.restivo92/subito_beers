package it.restivo.subito.beers.domain.repository

import it.restivo.subito.beers.data.remote.PunkApi
import it.restivo.subito.beers.domain.model.Beer
import it.restivo.subito.beers.utils.Constants.DEFAULT_PAGE_SIZE
import it.restivo.subito.beers.utils.Resource
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BeerRepository @Inject constructor(
    private val punkApi: PunkApi
) {

    suspend fun loadBeers(
        brewedBefore: String? = null,
        brewedAfter: String? = null,
        page: Int = 1,
        pageSize: Int = DEFAULT_PAGE_SIZE
    ): Resource<List<Beer>> {
        return try {
            val beers = punkApi.getBeers(
                brewedBefore = brewedBefore,
                brewedAfter = brewedAfter,
                page = page,
                pageSize = pageSize
            )
            Resource.Success(data = beers)
        } catch(e: IOException) {
            Resource.Error(
                error = "Couldn\'t reach server. Check your internet connection."
            )
        } catch(e: HttpException) {
            Resource.Error(
                error = "Something went wrong. Please try again."
            )
        }
    }
}