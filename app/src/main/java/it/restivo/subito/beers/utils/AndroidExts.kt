package it.restivo.subito.beers.utils

import android.content.Context
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.SnackbarResult
import it.restivo.subito.beers.R
import timber.log.Timber
import java.time.YearMonth
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*


val zone: ZoneId = ZoneId.of("Europe/Rome")
internal fun Date.yearMonthFromDate(): YearMonth {
    val dateTime: ZonedDateTime = toInstant().atZone(zone).withDayOfMonth(1)
    return YearMonth.from(dateTime)
}

internal fun YearMonth.toDate(): Date {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, monthValue - 1)
    return calendar.time
}

internal fun Date.toBrewedFormat(): String {
    val ym = yearMonthFromDate()
    val m = ym.monthValue.toString().padStart(2, '0')
    return "${m}-${ym.year}"
}

internal fun String.brewedYearMonth(): YearMonth? {
    val ym = this.split("-")
    Timber.d("$this - $ym")
    return if (ym.size == 2) { YearMonth.of(ym[1].toInt(), ym[0].toInt()) }
    else null
}

suspend fun SnackbarHostState.showRetryableSnackbar(
    ctx: Context,
    message: String,
): SnackbarResult {
    return showSnackbar(
        message = message,
        actionLabel = ctx.getString(R.string.retry),
        duration = SnackbarDuration.Indefinite
    )
}