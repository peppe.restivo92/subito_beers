package it.restivo.subito.beers.utils


interface Paginator<T> {

    suspend fun loadNextItems(restart: Boolean = false)
}