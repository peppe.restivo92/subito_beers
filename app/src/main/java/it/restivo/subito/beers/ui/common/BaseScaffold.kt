package it.restivo.subito.beers.ui.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import androidx.compose.ui.unit.Dp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.google.accompanist.insets.ui.TopAppBar


@Composable
fun BaseScaffold(
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    topBar: @Composable () -> Unit,
    content: @Composable () -> Unit
) {
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = topBar,
    ) { contentPadding ->
        Box(Modifier.padding(contentPadding)) {
            content()
        }
    }
}

@Composable
fun BasicTopAppBar(
    title: String,
    onNavigationIconClick: (() -> Unit)? = null,
    navActions: @Composable RowScope.() -> Unit = {},
    backgroundColor: Color = MaterialTheme.colors.primaryVariant,
    contentColor: Color = MaterialTheme.colors.onPrimary,
    elevation: Dp = AppBarDefaults.TopAppBarElevation
) {
    val defaultNavigationIcon: @Composable () -> Unit = {
        if (onNavigationIconClick != null) {
            IconButton(onClick = onNavigationIconClick) {
                Icon(Icons.Filled.ArrowBack, "")
            }
        }
    }

    val navigationIcon = if (onNavigationIconClick != null) defaultNavigationIcon else null

    // TopAppBar from accompanist-insets-ui
    TopAppBar(
        title = {
            Text(
                title,
                color = MaterialTheme.colors.onPrimary
            )
        },
        modifier = Modifier.fillMaxWidth(),
        navigationIcon = navigationIcon,
        backgroundColor = backgroundColor,
        contentPadding = rememberInsetsPaddingValues(
            LocalWindowInsets.current.statusBars,
            applyBottom = false,
        ),
        contentColor = contentColor,
        elevation = elevation,
        actions = navActions
    )
}

@Preview(showBackground = true)
@Composable
private fun BaseScreenPreviewLoading(
    @PreviewParameter(LoremIpsum::class, 50) loremIpsum: String
) {
    val topBar: @Composable () -> Unit = {
        BasicTopAppBar(title = "Titolo")
    }

    BaseScaffold(topBar = topBar) {
        Text(loremIpsum)
    }
}