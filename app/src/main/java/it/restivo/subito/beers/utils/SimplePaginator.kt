package it.restivo.subito.beers.utils

import it.restivo.subito.beers.utils.Constants.PUNK_API_FIRST_PAGE
import timber.log.Timber


class SimplePaginator<T>(
    private val onLoadUpdated: (Boolean) -> Unit,
    private val onRequest: suspend (nextPage: Int) -> Resource<List<T>>,
    private val onError: suspend (String) -> Unit,
    private val onSuccess: (items: List<T>) -> Unit
): Paginator<T> {

    private var page = PUNK_API_FIRST_PAGE

    override suspend fun loadNextItems(restart: Boolean) {
        onLoadUpdated(true)

        if (restart) {
            Timber.i("Restart from first page")
            page = PUNK_API_FIRST_PAGE
        }
        when(val result = onRequest(page)) {
            is Resource.Success -> {
                val items = result.data ?: emptyList()
                page++
                onLoadUpdated(false)
                onSuccess(items)
            }
            is Resource.Error -> {
                onLoadUpdated(false)
                onError(result.error ?: "Unknown error")
            }
        }
    }
}