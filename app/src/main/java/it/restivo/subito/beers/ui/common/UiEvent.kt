package it.restivo.subito.beers.ui.common

import it.restivo.subito.beers.utils.Event

sealed class UiEvent: Event() {
    data class ShowSnackbarWithRetry(val message: String) : UiEvent()
}