package it.restivo.subito.beers.ui.common


import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.dt.composedatepicker.*
import timber.log.Timber
import java.text.DateFormatSymbols
import java.util.*


@Composable
fun ComposeCalendar(
    startAt: Date? = null,
    minDate: Date? = null,
    maxDate: Date? = null,
    locale: Locale = Locale.getDefault(),
    title: String = "",
    listener: SelectDateListener,
    showOnlyMonth: Boolean = false,
    showOnlyYear: Boolean = false,
    themeColor:Color = MaterialTheme.colors.primary,
    negativeButtonTitle:String = "CANCEL",
    positiveButtonTitle:String = "OK"
) {
    if (showOnlyMonth && showOnlyYear) {
        throw IllegalStateException("'showOnlyMonth' and 'showOnlyYear' states cannot be true at the same time")
    } else {

        var minYear = 1970
        var minMonth = 0
        var maxYear = 2100
        var maxMonth = 11

        minDate?.let {
            val calendarMin = Calendar.getInstance()
            calendarMin.set(Calendar.DAY_OF_MONTH, 1)

            calendarMin.time = it
            minMonth = calendarMin.get(Calendar.MONTH)
            minYear = calendarMin.get(Calendar.YEAR)
        }
        maxDate?.let {
            val calendarMax = Calendar.getInstance()
            calendarMax.set(Calendar.DAY_OF_MONTH, 1)

            calendarMax.time = it
            maxMonth = calendarMax.get(Calendar.MONTH)
            maxYear = calendarMax.get(Calendar.YEAR)
        }

        val (height, setHeight) = remember {
            mutableStateOf(0)
        }

        val calendar = Calendar.getInstance(locale)
        calendar.set(Calendar.DAY_OF_MONTH, 1)

        var currentMonth = calendar.get(Calendar.MONTH)
        var currentYear = calendar.get(Calendar.YEAR)
        startAt?.let {
            calendar.time = it

            currentMonth = calendar.get(Calendar.MONTH)
            currentYear = calendar.get(Calendar.YEAR)
        }

        if (minYear > currentYear) {
            currentYear = minYear
        }
        if (maxYear < currentYear) {
            currentYear = maxYear
        }

        val months = (DateFormatSymbols(locale).shortMonths).toList()
        val monthList = months.mapIndexed { index, name ->
            MonthData(name = name, index = index)
        }
        val (selectedMonth, setMonth) = remember {
            mutableStateOf(
                MonthData(
                    name = DateFormatSymbols(locale).shortMonths[currentMonth],
                    index = currentMonth
                )
            )
        }
        val (selectedYear, setYear) = remember {
            mutableStateOf(currentYear)
        }
        val (showMonths, setShowMonths) = remember {
            mutableStateOf(!showOnlyYear)
        }

        val calendarDate = Calendar.getInstance()
        calendarDate.set(Calendar.DAY_OF_MONTH, 1)

        var selectedDate by remember {
            mutableStateOf(calendarDate.time)
        }

        LaunchedEffect(key1 = selectedYear, key2 = selectedMonth) {
            calendarDate.set(Calendar.YEAR, selectedYear)
            calendarDate.set(Calendar.MONTH, selectedMonth.index)
            selectedDate = calendarDate.time
        }
        LaunchedEffect(key1 = selectedYear) {
            Timber.d("Called minMonth: $minMonth, maxMonth: $maxMonth")
            Timber.d("Called minYear: $minYear, maxYear: $maxYear")

            if (selectedYear == minYear) {
                if (selectedMonth.index < minMonth) {
                    setMonth(monthList[minMonth])
                }
            }
            if (selectedYear == maxYear) {
                if (selectedMonth.index > maxMonth) {
                    setMonth(monthList[maxMonth])
                }
            }
        }

        Card(modifier = Modifier.fillMaxWidth(0.9f)) {
            Column(modifier = Modifier.fillMaxWidth()) {
                CalendarHeader(
                    selectedMonth = selectedMonth.name,
                    selectedYear = selectedYear,
                    showMonths = showMonths,
                    setShowMonths = setShowMonths,
                    title = title,
                    showOnlyMonth = showOnlyMonth,
                    showOnlyYear = showOnlyYear,
                    themeColor=themeColor)
                Crossfade(targetState = showMonths) {
                    when (it) {
                        true -> {
                            Timber.i("Called: $minMonth-$maxMonth, $minYear-$maxYear, selected: $selectedMonth-$selectedYear")
                            CalendarMonthView(
                                selectedMonth = selectedMonth,
                                setMonth = setMonth,
                                minMonth = minMonth,
                                maxMonth = maxMonth,
                                setShowMonths = setShowMonths,
                                minYear = minYear,
                                maxYear = maxYear,
                                selectedYear = selectedYear,
                                monthList = monthList,
                                setHeight = setHeight,
                                showOnlyMonth = showOnlyMonth,
                                themeColor=themeColor)
                        }
                        false -> CalendarYearView(
                            selectedYear = selectedYear,
                            setYear = setYear,
                            minYear = minYear,
                            maxYear = maxYear,
                            height = height,
                            themeColor=themeColor)
                    }
                }
                CalendarBottom(onPositiveClick = { listener.onDateSelected(selectedDate) },
                    onCancelClick = { listener.onCanceled() },
                    themeColor=themeColor,
                    negativeButtonTitle=negativeButtonTitle,
                    positiveButtonTitle=positiveButtonTitle)
            }
        }
    }
}
