package it.restivo.subito.beers.ui.beer_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import it.restivo.subito.beers.R
import it.restivo.subito.beers.domain.model.BeerMaskFilter
import it.restivo.subito.beers.ui.common.OutlinedDatePicker
import it.restivo.subito.beers.ui.theme.TextWhite
import it.restivo.subito.beers.utils.toBrewedFormat


@Composable
fun FilterBeersListDialog(
    _maskFilter: BeerMaskFilter,
    setShowFilter: (Boolean) -> Unit,
    onApply: (BeerMaskFilter) -> Unit
) {
    var maskFilter by remember {
        mutableStateOf(_maskFilter)
    }

    AlertDialog(
        modifier = Modifier
            .wrapContentHeight(),
        onDismissRequest = { setShowFilter(false) },
        title = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = stringResource(R.string.filters)
                )
                IconButton(onClick = { setShowFilter(false) }) {
                    Icon(imageVector = Icons.Outlined.Close, contentDescription = "close")
                }
            }
        },
        text = {
            Column {
                Text(text = stringResource(R.string.brewed), fontWeight = FontWeight.Bold)
                Spacer(modifier = Modifier.height(8.dp))
                Column {
                    OutlinedDatePicker(
                        label = R.string.after,
                        value = maskFilter.brewedAfter,
                        maxDate = maskFilter.brewedBefore
                    ) {
                        maskFilter = maskFilter.copy(
                            brewedAfter = it?.toBrewedFormat()
                        )
                    }
                    Spacer(modifier = Modifier.height(4.dp))
                    OutlinedDatePicker(
                        label = R.string.before,
                        value = maskFilter.brewedBefore,
                        minDate = maskFilter.brewedAfter
                    ) {
                        maskFilter = maskFilter.copy(
                            brewedBefore = it?.toBrewedFormat()
                        )
                    }
                }

            }
        },
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(46.dp)
                    .background(MaterialTheme.colors.primary),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Box(modifier = Modifier
                    .weight(1f)
                    .fillMaxSize()
                    .clickable {
                        onApply(BeerMaskFilter())
                        setShowFilter(false)
                    },
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = stringResource(id = R.string.reset).uppercase(),
                        color = TextWhite,
                        style = MaterialTheme.typography.button
                    )
                }
                Box(modifier = Modifier
                    .weight(1f)
                    .fillMaxSize()
                    .clickable {
                        onApply(maskFilter)
                        setShowFilter(false)
                    },
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = stringResource(id = R.string.apply).uppercase(),
                        color = TextWhite,
                        style = MaterialTheme.typography.button
                    )
                }
            }
        }
    )
}