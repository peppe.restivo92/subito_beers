package it.restivo.subito.beers.utils

object Constants {

    const val PUNK_API_FIRST_PAGE = 1
    const val DEFAULT_PAGE_SIZE = 25
}