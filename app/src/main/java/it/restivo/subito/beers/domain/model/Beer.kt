package it.restivo.subito.beers.domain.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class Beer(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "tagline") val tagline: String? = null,
    @Json(name = "first_brewed") val firstBrewed: String? = null,
    @Json(name = "description") val description: String? = null,
    @Json(name = "image_url") val imageUrl: String? = null
)
