package it.restivo.subito.beers.ui.beer_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import it.restivo.subito.beers.domain.model.Beer
import it.restivo.subito.beers.domain.model.BeerMaskFilter
import it.restivo.subito.beers.domain.repository.BeerRepository
import it.restivo.subito.beers.ui.common.PagingState
import it.restivo.subito.beers.ui.common.UiEvent
import it.restivo.subito.beers.utils.Event
import it.restivo.subito.beers.utils.SimplePaginator
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class BeerListViewModel @Inject constructor(
    private val beerRepository: BeerRepository
): ViewModel() {

    private var _pagingState = mutableStateOf<PagingState<Beer>>(PagingState())
    val pagingState: State<PagingState<Beer>> = _pagingState

    private var _maskFilter = mutableStateOf(BeerMaskFilter())
    val maskFilter: State<BeerMaskFilter> = _maskFilter

    private val _eventFlow = MutableSharedFlow<Event>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val paginator = SimplePaginator(
        onLoadUpdated = { isLoading ->
            _pagingState.value = pagingState.value.copy(
                isLoading = isLoading
            )
        },
        onRequest = { page ->
            beerRepository.loadBeers(
                brewedBefore = _maskFilter.value.brewedBefore,
                brewedAfter = _maskFilter.value.brewedAfter,
                page = page
            )
        },
        onSuccess = { beers ->
            _pagingState.value = pagingState.value.copy(
                items = pagingState.value.items + beers,
                endReached = beers.isEmpty(),
                isLoading = false
            )
        },
        onError = {
            if (!_pagingState.value.isLoading) _eventFlow.emit(UiEvent.ShowSnackbarWithRetry(it))
        }
    )

    init {
        loadNextBeers()
    }

    fun loadNextBeers() {
        viewModelScope.launch {
            paginator.loadNextItems()
        }
    }

    fun updateFilters(mask: BeerMaskFilter) {
        viewModelScope.launch {
            if (_maskFilter.value != mask) {
                _maskFilter.value = mask
                reloadData()
            }
        }
    }

    private suspend fun reloadData() {
        _pagingState.value = pagingState.value.copy(
            items = listOf()
        )
        paginator.loadNextItems(true)
    }
}