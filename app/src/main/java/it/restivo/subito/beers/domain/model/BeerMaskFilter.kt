package it.restivo.subito.beers.domain.model

data class BeerMaskFilter(
    val brewedAfter: String? = null,
    val brewedBefore: String? = null,
) {
    fun numberOfActiveFilters() = listOfNotNull(brewedAfter, brewedBefore).size
}