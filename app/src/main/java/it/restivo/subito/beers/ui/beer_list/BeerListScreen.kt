package it.restivo.subito.beers.ui.beer_list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FilterList
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import it.restivo.subito.beers.R
import it.restivo.subito.beers.ui.beer_list.components.BeerListItem
import it.restivo.subito.beers.ui.beer_list.components.FilterBeersListDialog
import it.restivo.subito.beers.ui.common.BaseScaffold
import it.restivo.subito.beers.ui.common.BasicTopAppBar
import it.restivo.subito.beers.ui.common.UiEvent
import it.restivo.subito.beers.utils.showRetryableSnackbar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun BeerListScreen(
    viewModel: BeerListViewModel = hiltViewModel()
) {
    val pagingState = viewModel.pagingState.value

    val (showFilterModal, setShowFilterModal) = remember {
        mutableStateOf(false)
    }

    val maskFilter by viewModel.maskFilter

    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()

    val context = LocalContext.current

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                is UiEvent.ShowSnackbarWithRetry -> {
                    coroutineScope.launch {
                        val snackBarResult = scaffoldState.snackbarHostState.showRetryableSnackbar(
                            context,
                            event.message
                        )
                        when (snackBarResult) {
                            SnackbarResult.Dismissed -> Timber.d("Dismissed")
                            SnackbarResult.ActionPerformed -> {
                                viewModel.loadNextBeers()
                            }
                        }
                    }
                }
            }
        }
    }

    BaseScaffold(
        scaffoldState = scaffoldState,
        topBar = {
            BasicTopAppBar(
                stringResource(id = R.string.app_name),
                navActions = {
                    IconButton(
                        enabled = !pagingState.isLoading,
                        onClick = { setShowFilterModal(true) }
                    ) {
                        Row {
                            Icon(
                                imageVector = Icons.Filled.FilterList,
                                contentDescription = "filters"
                            )
                            if (maskFilter.numberOfActiveFilters() > 0)
                                Text(text = "(${maskFilter.numberOfActiveFilters()})")
                        }
                    }
                }
            )
         },
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            LazyVerticalGrid(
                cells = GridCells.Adaptive(minSize = 500.dp),
                contentPadding = PaddingValues(horizontal = 8.dp, vertical = 8.dp),
                verticalArrangement = Arrangement.spacedBy(2.dp),
                horizontalArrangement = Arrangement.spacedBy(2.dp)
            ) {
                items(pagingState.items.size) { i ->
                    val beer = pagingState.items[i]
                    if (i >= pagingState.items.size - 1 && !pagingState.endReached && !pagingState.isLoading) {
                        viewModel.loadNextBeers()
                    }
                    BeerListItem(beer = beer)
                }
            }
            if (pagingState.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.BottomCenter)
                        .padding(bottom = 8.dp)
                        .clip(RoundedCornerShape(50))
                        .background(Color.White)

                )
            }
            if (showFilterModal) {
                FilterBeersListDialog(
                    _maskFilter = maskFilter,
                    setShowFilter = setShowFilterModal,
                    onApply = viewModel::updateFilters
                )
            }
        }
    }
}