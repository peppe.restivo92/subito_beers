package it.restivo.subito.beers

import io.mockk.Called
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import it.restivo.subito.beers.domain.model.Beer
import it.restivo.subito.beers.domain.model.BeerMaskFilter
import it.restivo.subito.beers.domain.repository.BeerRepository
import it.restivo.subito.beers.ui.beer_list.BeerListViewModel
import it.restivo.subito.beers.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class BeersViewModelUnitTest {

    private lateinit var viewModel: BeerListViewModel

    @MockK(relaxed = false)
    lateinit var repository: BeerRepository

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun setup() {
        MockKAnnotations.init(this)

        coEvery {
            repository.loadBeers(any(), any(), any(), any())
        } returns Resource.Success(listOf())
    }

    @Test
    fun `when viewmodel is initialized, test initial data`() = testCoroutineRule.runBlockingTest {
        viewModel = BeerListViewModel(repository)

        coVerify {
            repository.loadBeers(page = 1)
        }
    }

    @Test
    fun `when loadNextBeers is initialized, test initial data`() = testCoroutineRule.runBlockingTest {
        viewModel = BeerListViewModel(repository)

        viewModel.loadNextBeers()
        coVerify {
            repository.loadBeers(page = 2)
        }
    }

    @Test
    fun `when loadNextBeers is called, test with filter data updates`() = testCoroutineRule.runBlockingTest {
        viewModel = BeerListViewModel(repository)

        viewModel.loadNextBeers()
        coVerify {
            repository.loadBeers(page = 2)
        }

        viewModel.updateFilters(BeerMaskFilter(brewedAfter = "01-2007"))
        viewModel.loadNextBeers()

        coVerify {
            repository.loadBeers(brewedAfter = "01-2007", page = 1)
            repository.loadBeers(brewedAfter = "01-2007", page = 2)
        }

        viewModel.updateFilters(BeerMaskFilter(brewedAfter = "01-2007", brewedBefore = "01-2022"))
        viewModel.loadNextBeers()

        coVerify {
            repository.loadBeers(brewedAfter = "01-2007", page = 3) wasNot Called
            repository.loadBeers(brewedBefore = "01-2022", brewedAfter = "01-2007", 1)
            repository.loadBeers(brewedBefore = "01-2022", brewedAfter = "01-2007", 2)
        }
        Assert.assertFalse(viewModel.pagingState.value.isLoading)
    }

    @Test
    fun `when loadNextBeers is called, test paging state with filter data updates`() = testCoroutineRule.runBlockingTest {
        coEvery {
            repository.loadBeers(page = 1)
        } returns Resource.Success(listOf(Beer(id = 0, name = "beer0")))
        coEvery {
            repository.loadBeers(brewedAfter = "01-2007", page = 1)
        } returns Resource.Success(listOf(Beer(id = 1, name = "beer1")))
        coEvery {
            repository.loadBeers(brewedAfter = "01-2007", page = 2)
        } returns Resource.Success(listOf(Beer(id = 1, name = "beer2")))
        coEvery {
            repository.loadBeers(brewedAfter = "01-2007", page = 3)
        } returns Resource.Success(listOf())

        viewModel = BeerListViewModel(repository)
        viewModel.pagingState.value.apply {
            Assert.assertEquals(1, items.size)
            Assert.assertFalse(endReached)
            Assert.assertFalse(isLoading)
        }

        viewModel.updateFilters(BeerMaskFilter(brewedAfter = "01-2007"))
        viewModel.pagingState.value.apply {
            Assert.assertEquals(1, items.size)
            Assert.assertFalse(endReached)
            Assert.assertFalse(isLoading)
        }

        viewModel.loadNextBeers()
        viewModel.pagingState.value.apply {
            Assert.assertEquals(2, items.size)
            Assert.assertFalse(endReached)
            Assert.assertFalse(isLoading)
        }

        viewModel.loadNextBeers()
        viewModel.pagingState.value.apply {
            Assert.assertEquals(2, items.size)
            Assert.assertTrue(endReached)
            Assert.assertFalse(isLoading)
        }

        coVerify {
            repository.loadBeers(brewedAfter = "01-2007", page = 1)
            repository.loadBeers(brewedAfter = "01-2007", page = 2)
            repository.loadBeers(brewedAfter = "01-2007", page = 3)
        }
    }

    @Test
    fun `when no filter changed, test no loadnextbeers is called`() = testCoroutineRule.runBlockingTest {
        viewModel = BeerListViewModel(repository)
        viewModel.updateFilters(BeerMaskFilter(brewedAfter = "01-2007"))
        viewModel.loadNextBeers()

        viewModel.updateFilters(BeerMaskFilter(brewedAfter = "01-2007")) // this should not produce next load beers
        viewModel.loadNextBeers()

        coVerify {
            repository.loadBeers(page = 1)
            repository.loadBeers(brewedAfter = "01-2007", page = 1)
            repository.loadBeers(brewedAfter = "01-2007", page = 2)
            repository.loadBeers(brewedAfter = "01-2007", page = 3)
            repository.loadBeers(brewedAfter = "01-2007", page = 4) wasNot Called
        }
    }
}