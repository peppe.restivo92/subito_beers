package it.restivo.subito.beers.data.remote


import it.restivo.subito.beers.domain.model.Beer
import retrofit2.http.GET
import retrofit2.http.Query

interface PunkApi {

    @GET("beers")
    suspend fun getBeers(
        @Query("brewed_before") brewedBefore: String? = null,
        @Query("brewed_after") brewedAfter: String? = null,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): List<Beer>

    companion object {
        const val BASE_URL = "https://api.punkapi.com/v2/"
    }
}